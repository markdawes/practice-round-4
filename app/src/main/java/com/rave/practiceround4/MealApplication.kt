package com.rave.practiceround4

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Meal application class for dagger.
 *
 * @constructor Create empty Meal application
 */
@HiltAndroidApp
class MealApplication : Application()
