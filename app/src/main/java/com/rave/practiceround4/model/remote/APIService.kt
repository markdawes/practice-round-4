package com.rave.practiceround4.model.remote

import com.rave.practiceround4.model.remote.dtos.CategoryResponse
import retrofit2.http.GET

/**
 * Api service to get category endpoints.
 *
 * @constructor Create empty Api service
 */
interface APIService {

    @GET(CATEGORY_ENDPOINT)
    suspend fun getMealCategories(): CategoryResponse

    companion object {
        private const val CATEGORY_ENDPOINT = "categories.php"
    }
}
