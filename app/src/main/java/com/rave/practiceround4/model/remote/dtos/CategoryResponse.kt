package com.rave.practiceround4.model.remote.dtos

import kotlinx.serialization.Serializable

@Serializable
data class CategoryResponse(
    val categories: List<CategoryDTO>
)
